// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace LearningMacGUI
{
    [Register ("ViewController")]
    partial class ViewController
    {
        [Outlet]
        AppKit.NSButton btn_clickMe { get; set; }

        [Outlet]
        AppKit.NSTextField ClickedLabel { get; set; }

        [Action ("ButtonClicked:")]
        partial void ButtonClicked (AppKit.NSButton sender);
        
        void ReleaseDesignerOutlets ()
        {
            if (btn_clickMe != null) {
                btn_clickMe.Dispose ();
                btn_clickMe = null;
            }

            if (ClickedLabel != null) {
                ClickedLabel.Dispose ();
                ClickedLabel = null;
            }
        }
    }
}
